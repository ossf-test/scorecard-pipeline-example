
# Running OpenSSF Scorecard in GitLab pipelines

This repo is an example of running Scorecard in GitLab pipelines. It uses the `.gitlab-ci.yml` file format (see [GitLab Docs](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html) for more information.)

## Authenticating to GitLab API

### Step 1. Create Token
You must create a [GitLab Access Token](https://gitlab.com/-/profile/personal_access_tokens) to run all Scorecard checks. The default `CI_JOB_TOKEN` does not have GitLab API permissions, so you must create a new token with the following permissions:
* `read_api`
* `read_user`
* `read_repository`

### Step 2. Set token as environment variable
Copy the GitLab PAT from the previous step to your clipboard. In your repo's settings page (Settings > CI/CD > Variables) create a new variable and check `Protected` and `Masked` options, since this is a secret token.

### Deploy
You should now see Scorecard being run (Build > Pipelines). You can also build automation around Scorecard results using [`Scorecard-Attestor`](https://github.com/ossf/scorecard/tree/main/attestor) or by piping the output of Scorecard with the `--json` or `--raw` flags to another tool, like `jq`.